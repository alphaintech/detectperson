﻿#include "opencv2/highgui/highgui.hpp"
#include <opencv2/opencv.hpp>
#include <iostream>

using namespace cv;
using namespace std;


int main(){
	Mat img;
	img = Mat::zeros(Size(500, 500), CV_8UC1);

	//namedWindow("MYWINDOW", CV_WINDOW_AUTOSIZE);
	//imshow("MYWINDOW", img);
	
	circle(img, Point(250, 250), 100, Scalar(255), 1, 8, 0);
	rectangle(img, Point(10,10), Point(150,150), Scalar(255), 2, 8, 0);
	//line(img, Point(40), Point(220), Scalar(255), 1, 8, 0);
	imshow("MYWINDOW", img);

	waitKey(0);
	cvvDestroyWindow("MYWINDOW");

	return 0;
}