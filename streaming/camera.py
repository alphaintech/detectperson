import cv2

class VideoCamera(object):
    def __init__(self):
       
        #self.video = cv2.VideoCapture(-1)
        self.video = cv2.VideoCapture('testvideo.mp4')
    
    def __del__(self):
        self.video.release()
    
    def get_frame(self):
        success, image = self.video.read()
       #encode it into JPEG in order to correctly display the
        ret, jpeg = cv2.imencode('.jpg', image)
        return jpeg.tobytes()
